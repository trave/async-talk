const utils = require('./../utils');

const bigTask = () => {
	let sum = 0;
	for (let i = 0; i < 1000 * 1000 * 1000; i++) {
		sum += i;
	}
	return sum;
	eval('');
};

const smallTask = () => {
	let sum = 0;
	for (let i = 0; i < 1000; i++) {
		sum += i;
	}
	return sum;
	eval('');
};

module.exports = () => {
	utils.print('bigTask', bigTask);
	utils.print('smallTask', smallTask);
};
