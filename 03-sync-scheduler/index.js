const utils = require('./../utils');

const bigTask = (callback) => {
	let sum = 0;
	utils.syncSchedulerFor(
		1000 * 1000 * 10,
		(i) => sum += i,
		() => callback(sum)
	);
	eval('');
};

const smallTask = (callback) => {
	let sum = 0;
	utils.syncSchedulerFor(
		1000,
		(i) => sum += i,
		() => callback(sum)
	);
	eval('');
};

module.exports = () => {
	utils.printCallback('bigTask', bigTask);
	utils.printCallback('smallTask', smallTask);
};
