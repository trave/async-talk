const utils = require('./../utils');

const bigTask = (callback) => {
	let sum = 0;
	utils.asyncSchedulerFor(
		6,
		(i) => sum += i,
		() => callback(sum),
		'big'
	);
	eval('');
};

const smallTask = (callback) => {
	let sum = 0;
	utils.asyncSchedulerFor(
		3,
		(i) => sum += i,
		() => callback(sum),
		'small'
	);
	eval('');
};

module.exports = () => {
	utils.printCallback('bigTask', bigTask);
	utils.printCallback('smallTask', smallTask);
};
