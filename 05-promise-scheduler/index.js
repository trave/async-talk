const utils = require('./../utils');

const bigTask = (callback) => {
	let sum = 0;
	utils.promiseSchedulerFor(
		1000 * 1000 * 10,
		(i) => sum += i,
		() => callback(sum)
	);
	eval('');
};

const smallTask = (callback) => {
	let sum = 0;
	utils.promiseSchedulerFor(
		1000,
		(i) => sum += i,
		() => callback(sum)
	);
	eval('');
};

module.exports = () => {
	utils.printCallback('bigTask', bigTask);
	setImmediate(() => console.log('tick'));
	utils.printCallback('smallTask', smallTask);
};
