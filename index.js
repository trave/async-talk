const fs = require('fs');
const path = require('path');

const works = {};

fs.readdirSync(__dirname)
	.forEach((file) => {
		const dirpath = path.join(__dirname, file);
		const filepath = path.join(dirpath, 'index.js');
		if (/^\d+\-.+/.test(file) && fs.lstatSync(dirpath).isDirectory() && fs.existsSync(filepath)) {
			const taskNmae = 'work' + file.replace(/\-([a-z])/g, (str, m) => m.toUpperCase());
			works[taskNmae] = require(filepath);
		}
	});


console.log(Object.keys(works));

module.exports = works;