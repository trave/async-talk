module.exports = {
	now: require('./utils/now'),
	print: require('./utils/print'),
	printCallback: require('./utils/print-callback'),
	syncFor: require('./utils/sync-for'),
	asyncFor: require('./utils/async-for'),
	syncSchedulerFor: require('./utils/sync-scheduler-for'),
	asyncSchedulerFor: require('./utils/async-scheduler-for'),
	promiseSchedulerFor: require('./utils/promise-scheduler-for'),
};
