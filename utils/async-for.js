module.exports = (end, body, callback) => {
	let i = 0;

	const step = () => {
		if (i < end) {
			body(i);
			i += 1;
			setImmediate(step);
		} else {
			callback();
		}
	};

	setImmediate(step);
};
