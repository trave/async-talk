const now = require('./now');

module.exports = (title, work) => {
	const start = now();
	work((result) => {
		const end = now();
		console.log(
			title,
			'task time',
			((end - start) / 1000).toFixed(3) + 'ms',
			'result is',
			result
		);
	});
};
