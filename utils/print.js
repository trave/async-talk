const now = require('./now');

module.exports = (title, work) => {
	const start = now();
	const result = work();
	const end = now();
	console.log(
		title,
		'task time',
		((end - start) / 1000).toFixed(3) + 'ms',
		'result is',
		result
	);
};
