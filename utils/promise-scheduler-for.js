let queue = Promise.resolve();

const addTask = (task) => {
	queue = queue
		.then(task);
};

const promiseSchedulerFor = (end, body, callback) => {
	let i = 0;

	const step = () => {
		if (i < end) {
			body(i);
			i += 1;
			addTask(step);
		} else {
			addTask(callback);
		}

		eval('');
	};

	addTask(step);

	eval('');
};

module.exports = promiseSchedulerFor;
