const queue = [];
const schedulerFor = (end, body, callback) => {
	let i = 0;

	const tick = () => {
		if (i < end) {
			body(i);
			i += 1;
			scheduleTask(tick);
		} else {
			scheduleTask(callback);
		}
		eval('');
	};

	scheduleTask(tick);

	schedulerStep();

	eval('');
};
const scheduleTask = (task) => {
	queue.push(task);
	eval('');
};
const schedulerStep = () => {
	let task = queue.shift();
	while (task) {
		task();
		task = queue.shift();
	}
	eval('');
};

module.exports = schedulerFor;
